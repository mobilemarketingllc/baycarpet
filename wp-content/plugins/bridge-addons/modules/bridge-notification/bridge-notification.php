<?php

class BridgeNotificationModule extends FLBuilderModule {

	public function __construct()
	{
		parent::__construct(array(
			'name'              => __( 'Notification', 'bb-bridge' ),
			'description'       => __( 'Addon to display your notifications.', 'bb-bridge' ),
			'category'		    => __( 'Bridge Modules', 'bb-bridge' ),
			'dir'               => BB_BRIDGE_DIR . 'modules/bridge-notification/',
			'url'               => BB_BRIDGE_URL . 'modules/bridge-notification/',
			'editor_export'     => true,
			'enabled'           => true,
			'partial_refresh'   => true,
		));
	}
}


FLBuilder::register_module( 'BridgeNotificationModule', array(
	'general'       => array(
		'title'         => __( 'General', 'bb-bridge' ),
		'sections'      => array(
			'general'       => array(
				'title'         => __( 'Notification', 'fl-builder' ),
				'fields'        => array(
					'icon'           => array(
						'type'           => 'icon',
						'label'          => __( 'Icon', 'bb-bridge' ),
						'show_remove'    => true,
					),
					'title'          => array(
						'type'           => 'text',
						'label'          => __( 'Title', 'bb-bridge' ),
						'default'        => 'Success!',
						'placeholder'    => 'Success!',
						'size'           => '30',
					),
					'description'    => array(
						'type'           => 'textarea',
						'label'          => __( 'Description', 'bb-bridge' ),
						'default'        => '',
						'rows'           => '6',
					),
				),
			),
		),
	),
	'style'         => array(
		'title'         => __( 'Style', 'bb-bridge' ),
		'sections'      => array(
			'background'    => array(
				'title'         => __( 'Background', 'bb-bridge' ),
				'fields'        => array(
					'background'         => array(
						'type'               => 'select',
						'label'              => __( 'Type', 'bb-bridge' ),
						'default'            => 'color',
						'options'            => array(
							'none'              => __( 'None', 'bb-bridge' ),
							'color'             => __( 'Color', 'bb-bridge' ),
						),
						'toggle'             => array(
							'color'              => array(
								'fields'             => array( 'bg_color' ),
							),
						),
					),
					'bg_color'           => array(
						'type'               => 'color',
						'label'              => __( 'Background Color', 'bb-bridge' ),
						'default'            => '00aa00',
						'show_reset'         => true,
						'show_alpha'         => true,
					),			
				)
			),
			'colors'        => array(
				'title'         => __( 'Colors', 'bb-bridge' ),
				'fields'        => array(
					'color'              => array( 
						'type'               => 'color',
						'label'              => __( 'Text Color', 'bb-bridge' ),
						'default'            => 'ffffff',
						'show_reset'         => true,
					),
					'icon_color'         => array( 
						'type'               => 'color',
						'label'              => __( 'Icon Color', 'bb-bridge' ),
						'default'            => 'ffffff',
						'show_reset'         => true,
					),
				),
			),
			'corners'        => array(
				'title'          => __( 'Corner Radius', 'bb-bridge' ),
				'fields'         => array(
					'top_left'               => array(
						'type'                   => 'text',
						'label'                  => __( 'Top Left', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'top_right'              => array(
						'type'                   => 'text',
						'label'                  => __( 'Top Right', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'bottom_left'            => array(
						'type'                   => 'text',
						'label'                  => __( 'Bottom Left', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'bottom_right'           => array(
						'type'                   => 'text',
						'label'                  => __( 'Bottom Right', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
				),
			),
			'border'         => array(
				'title'          => __( 'Border', 'bb-bridge' ),
				'fields'         => array(
					'border_style'       => array(
						'type'               => 'select',
						'label'              => __( 'Type', 'bb-bridge' ),
						'default'            => 'none',
						'options'            => array(
							'none'               => __( 'None', 'bb-bridge' ),
							'solid'              => __( 'Solid', 'bb-bridge' ),
							'dashed'             => __( 'Dashed', 'bb-bridge' ),
							'dotted'             => __( 'Dotted', 'bb-bridge' ),							
							'double'             => __( 'Double', 'bb-bridge' ),
						),
						'toggle'             => array(
							'solid'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'dashed'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'dotted'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'double'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
						),
					),
					'border_color'       => array( 
						'type'               => 'color',
						'label'              => __( 'Color', 'bb-bridge' ),
						'default'            => '',
						'show_reset'         => true,
						'show_alpha'         => true,
					),
					'border_width'       => array( 
						'type'               => 'dimension',
						'label'              => __( 'Width', 'bb-bridge' ),
						'description'        => 'px',
						'default'            => '',
						'placeholder'        => '0',
					),					
				),
			),
			'icon'    => array(
				'title'         => __( 'Icon', 'bb-bridge' ),
				'fields'        => array(
					'display_icon'       => array(
						'type'               => 'select',
						'label'              => __( 'Display Icon', 'bb-bridge' ),
						'default'            => 'yes',
						'options'            => array(
							'yes'                => __( 'Yes', 'bb-bridge' ),
							'no'                 => __( 'No', 'bb-bridge' ),
						),
					),			
				)
			),
		),
	),
));