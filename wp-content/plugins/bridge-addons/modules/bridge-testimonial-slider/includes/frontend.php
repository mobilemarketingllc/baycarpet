<div class="bb-bridge-tetimonial-slider">

	<div class="testim-content">
		<?php $i = 0; ?>
		<?php foreach ($settings->testimonials as $testimonial): ?>
		<?php $i++; ?>

			<div class="<?php if ($i == "1") { echo "active"; } ?>">
				<div><img src="<?php echo $testimonial->person_image_src; ?>" alt="<?php echo $testimonial->person_name; ?>"></div>
				<h3><?php echo $testimonial->person_name; ?></h3>
				<span><?php echo $testimonial->person_position; ?></span>
				<?php echo $testimonial->testimonial_text; ?>
			</div>
		
		<?php endforeach; ?>
	</div>
	
	<span class="bb-bridge-arrow testim-arrow-right"></span>
	<span class="bb-bridge-arrow testim-arrow-left"></span>

	<ul class="testim-dots">
		<?php $i = 0; ?>
		<?php foreach ($settings->testimonials as $testimonial): ?>
		<?php $i++; ?>			
		<li class="testim-dot <?php if ($i == "1") { echo "active"; } ?>"></li>
		<?php endforeach; ?>
	</ul>

</div>