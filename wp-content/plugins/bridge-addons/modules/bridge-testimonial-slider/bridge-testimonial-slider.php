<?php

class BridgeTestimonialSliderModule extends FLBuilderModule {

	public function __construct()
	{
		parent::__construct(array(
			'name'              => __( 'Testimonial Slider', 'bb-bridge' ),
			'description'       => __( 'Addon to display your testimonials in slider.', 'bb-bridge' ),
			'category'          => __( 'Bridge Modules', 'bb-bridge' ),
			'dir'               => BB_BRIDGE_DIR . 'modules/bridge-testimonial-slider/',
			'url'               => BB_BRIDGE_URL . 'modules/bridge-testimonial-slider/',
			'editor_export'     => true, // Defaults to true and can be omitted.
			'enabled'           => true, // Defaults to true and can be omitted.
			'partial_refresh'   => true,
		));
	}
}


FLBuilder::register_module( 'BridgeTestimonialSliderModule', array(
	'general'       => array( // Tab
		'title'         => __( 'General', 'bb-bridge' ), // Tab title
		'sections'      => array( // Tab Sections
			'general'       => array( // Section
				'fields'        => array( // Section Fields
					'testimonials'     => array(
						'type'          => 'form',
						'label'         => __( 'Testimonial', 'bb-bridge' ),
						'form'          => 'testimonials',
						'multiple'      => true
					),
				)
			)
		)
	),
	'style'         => array(
		'title'         => __( 'Style', 'bb-bridge' ),
		'sections'      => array(
			'image'         => array(
				'title'         => __( 'Image', 'bb-bridge' ),
				'fields'        => array(
					'image'              => array(
						'type'               => 'select',
						'label'              => __( 'Image Style', 'bb-bridge' ),
						'default'            => 'circle',
						'options'            => array(
							'default'            => __( 'Default', 'bb-bridge' ),
							'circle'             => __( 'Circle', 'bb-bridge' ),
						),
					),		
				)
			),
			'arrows'        => array(
				'title'         => __( 'Arrows', 'bb-bridge' ),
				'fields'        => array(
					'arrows'              => array(
						'type'               => 'select',
						'label'              => __( 'Display Arrows', 'bb-bridge' ),
						'default'            => 'yes',
						'options'            => array(
							'yes'                => __( 'Yes', 'bb-bridge' ),
							'no'                 => __( 'No', 'bb-bridge' ),
						),
					),		
				)
			),
			'dots'          => array(
				'title'         => __( 'Dots', 'bb-bridge' ),
				'fields'        => array(
					'dots'               => array(
						'type'               => 'select',
						'label'              => __( 'Display Dots', 'bb-bridge' ),
						'default'            => 'yes',
						'options'            => array(
							'yes'                => __( 'Yes', 'bb-bridge' ),
							'no'                 => __( 'No', 'bb-bridge' ),
						),
					),		
				)
			),
			'background'    => array(
				'title'         => __( 'Background', 'bb-bridge' ),
				'fields'        => array(
					'background'         => array(
						'type'               => 'select',
						'label'              => __( 'Type', 'bb-bridge' ),
						'default'            => 'none',
						'options'            => array(
							'none'              => __( 'None', 'bb-bridge' ),
							'color'             => __( 'Color', 'bb-bridge' ),
						),
						'toggle'             => array(
							'color'              => array(
								'fields'             => array( 'bg_color' ),
							),
						),
					),
					'bg_color'           => array(
						'type'               => 'color',
						'label'              => __( 'Background Color', 'bb-bridge' ),
						'default'            => 'fafafa',
						'show_reset'         => true,
						'show_alpha'         => true,
					),			
				)
			),
			'colors'        => array(
				'title'         => __( 'Colors', 'bb-bridge' ),
				'fields'        => array(
					'color'              => array( 
						'type'               => 'color',
						'label'              => __( 'Person Name', 'bb-bridge' ),
						'default'            => 'd17d00',
					),
					'position_color'     => array( 
						'type'               => 'color',
						'label'              => __( 'Person Position', 'bb-bridge' ),
						'default'            => '000000',
					),
					'text_color'         => array( 
						'type'               => 'color',
						'label'              => __( 'Text', 'bb-bridge' ),
						'default'            => '000000',
					),
					'dots_color'         => array( 
						'type'               => 'color',
						'label'              => __( 'Dots Color', 'bb-bridge' ),
						'default'            => 'd17d00',
					),
					'arrows_color'       => array( 
						'type'               => 'color',
						'label'              => __( 'Arrows Color', 'bb-bridge' ),
						'default'            => 'eeeeee',
					),
					'arrows_hover_color' => array( 
						'type'               => 'color',
						'label'              => __( 'Arrows Hover Color', 'bb-bridge' ),
						'default'            => 'd17d00',
					),
				),
			),
			'corners'        => array(
				'title'          => __( 'Corner Radius', 'bb-bridge' ),
				'fields'         => array(
					'top_left'               => array(
						'type'                   => 'text',
						'label'                  => __( 'Top Left', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'top_right'              => array(
						'type'                   => 'text',
						'label'                  => __( 'Top Right', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'bottom_left'            => array(
						'type'                   => 'text',
						'label'                  => __( 'Bottom Left', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
					'bottom_right'           => array(
						'type'                   => 'text',
						'label'                  => __( 'Bottom Right', 'bb-bridge' ),
						'description'            => 'px',
						'default'                => '',
						'size'                   => '5',
						'placeholder'            => '0'
					),
				),
			),
			'border'         => array(
				'title'          => __( 'Border', 'bb-bridge' ),
				'fields'         => array(
					'border_style'       => array(
						'type'               => 'select',
						'label'              => __( 'Type', 'bb-bridge' ),
						'default'            => 'none',
						'options'            => array(
							'none'               => __( 'None', 'bb-bridge' ),
							'solid'              => __( 'Solid', 'bb-bridge' ),
							'dashed'             => __( 'Dashed', 'bb-bridge' ),
							'dotted'             => __( 'Dotted', 'bb-bridge' ),							
							'double'             => __( 'Double', 'bb-bridge' ),
						),
						'toggle'             => array(
							'solid'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'dashed'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'dotted'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
							'double'              => array(
								'fields'             => array( 'border_color', 'border_width' ),
							),
						),
					),
					'border_color'       => array( 
						'type'               => 'color',
						'label'              => __( 'Color', 'bb-bridge' ),
						'default'            => '',
						'show_reset'         => true,
						'show_alpha'         => true,
					),
					'border_width'       => array( 
						'type'               => 'dimension',
						'label'              => __( 'Width', 'bb-bridge' ),
						'description'        => 'px',
						'default'            => '',
						'placeholder'        => '0',
					),					
				),
			),
		),
	),
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form( 'testimonials', array(
	'title' => __( 'Testimonials', 'bb-bridge' ),
	'tabs'  => array(
		'general'      => array( // Tab
			'title'         => __( 'General', 'bb-bridge' ), // Tab title
			'sections'      => array( // Tab Sections
				'general'       => array( // Section
					'title'         => '', // Section Title
					'fields'        => array( // Section Fields
						'person_image'       => array(
							'type'              => 'photo',
							'label'             => __( 'Person Image', 'bb-bridge' ),
						),
						'person_name'        => array(
							'type'              => 'text',
							'label'             => __( 'Person Name', 'bb-bridge' ),
							'size'              => '30',
						),
						'person_position'    => array(
							'type'              => 'text',
							'label'             => __( 'Person Position', 'bb-bridge' ),
							'size'              => '30',
						),
						'testimonial_text'   => array(
							'type'              => 'editor',
							'label'             => __( 'Testimonial Text', 'bb-bridge' ),
							'default'           => 'Testimonial text',
							'rows'              => 10
						),
					)
				)
			)
		),
	)
));