<?php

/**
 * Add support for SEO plugins
 * @since 2.2.4
 */
class FLBuilderSeoPlugins {

	function __construct() {

		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_head', array( $this, 'remove_yoast_meta_box_on_edit' ), 999 );

		add_filter( 'wpseo_sitemap_exclude_post_type', array( $this, 'sitemap_exclude_post_type' ), 10, 2 );
		add_filter( 'wpseo_sitemap_exclude_taxonomy', array( $this, 'sitemap_exclude_taxonomy' ), 10, 2 );
		add_filter( 'manage_edit-fl-builder-template_columns', array( $this, 'remove_columns' ) );

		add_filter( 'rank_math/sitemap/excluded_post_types', array( $this, 'rankmath_types' ) );
	}

	function init() {
		global $pagenow;
		if ( FLBuilderAJAX::doing_ajax() || 'post.php' !== $pagenow ) {
			return;
		}
		if ( defined( 'WPSEO_VERSION' ) ) {
			$this->enqueue_script( 'yoast' );
		} elseif ( class_exists( 'RankMath' ) ) {
			$this->enqueue_script( 'rankmath' );
		}
	}

	function rankmath_types( $post_types ) {
		unset( $post_types['fl-builder-template'] );
		return $post_types;
	}

	function remove_columns( $columns ) {

		// remove the Yoast SEO columns
		unset( $columns['wpseo-score'] );
		unset( $columns['wpseo-title'] );
		unset( $columns['wpseo-links'] );
		unset( $columns['wpseo-metadesc'] );
		unset( $columns['wpseo-focuskw'] );
		unset( $columns['wpseo-score-readability'] );
		// RankMath columns
		unset( $columns['rank_math_seo_details'] );
		unset( $columns['rank_math_title'] );
		unset( $columns['rank_math_description'] );
		return $columns;
	}

	function remove_yoast_meta_box_on_edit() {
		remove_meta_box( 'wpseo_meta', 'fl-builder-template', 'normal' );
	}

	function sitemap_exclude_post_type( $value, $post_type ) {
		if ( 'fl-builder-template' === $post_type ) {
			return true;
		}
		return $value;
	}

	function sitemap_exclude_taxonomy( $value, $taxonomy ) {
		if ( 'fl-builder-template-category' === $taxonomy ) {
			return true;
		}
		return $value;
	}

	function enqueue_script( $plugin ) {

		if ( ! isset( $_GET['post'] ) ) {
			return false;
		}

		$post_id = $_GET['post'];

		$post_type = get_post_type( $post_id );

		if ( in_array( $post_type, array( 'fl-theme-layout', 'fl-builder-template' ) ) ) {
				return false;
		}

		if ( 'yoast' === $plugin ) {
			$deps = array( 'yoast-seo-post-scraper' );
		} else {
			$deps = array( 'rank-math-post-metabox' );
		}

		$data = $this->content_data();

		if ( $data ) {
			wp_enqueue_script( 'bb-seo-scripts', FL_BUILDER_SEO_PLUGINS_URL . "js/plugin-$plugin.js", $deps, false, true );
			wp_localize_script( 'bb-seo-scripts', 'bb_seo_data', array( 'content' => $data ) );
		}
	}

	function content_data() {

		if ( ! isset( $_GET['post'] ) ) {
			return false;
		}

		$id = $_GET['post'];

		if ( ! get_post_meta( $id, '_fl_builder_enabled', true ) ) {
			return false;
		}
		ob_start();
		echo do_shortcode( "[fl_builder_insert_layout id=$id]" );
		$data = ob_get_clean();
		ob_end_flush();
		return str_replace( PHP_EOL, '', $data );
	}

}

new FLBuilderSeoPlugins();
