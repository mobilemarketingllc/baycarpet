<?php
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'mmdev');

/** MySQL database password */
define('DB_PASSWORD', 'mmdev@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'g@yN-[>M{wy-QG~@Bo6Idv!aIz8.jWDavSpjsHXnZ?$Ex:s<+;68:se=~UFlZBa)');
define('SECURE_AUTH_KEY',  'a?+lY9/;P(6Xlv-`Og)R*#-MBEWU{<+vUd;^eJsCcIN|0HXz%rw~ZaENS%@i>-7@');
define('LOGGED_IN_KEY',    'BWX^lm:%(5pduBc0(IBo~^sMYwu4rSN_lW`%5c-$7>?|kXbOx 5Ns&gnkME5f]Qj');
define('NONCE_KEY',        'S88~ND.~{5Nu<uA-u+=ik%seU09/D_ .dO|xIS{ ?<mNz@tg)V|Rcn1sg42SO60N');
define('AUTH_SALT',        ']6 =*edQWBOsbIpnof{Xf-C>hJG$ -k Vh;9] IM9J}+wh7W+1A5pw;g3WG6)d78');
define('SECURE_AUTH_SALT', 'V}Aj0~(UpM5L K>>`Xj}2O$CJPXD/2.r3%C7e9fj#^/i]$wp#MC{y2E%Wg-Lz/ed');
define('LOGGED_IN_SALT',   ':-!H2by6NV7#9|@r{GR-sS/kJT||}d5Atv$Wm-!)n|J^GmAPrye&Zbj|Sxd2 T{h');
define('NONCE_SALT',       'fy$5+A|jkt5_2Q2y375w%i_&q<-GayFb%-tlh{z0;5*={g(.tkMQ9H_MK0J5N3/X');
/**#@-*/
 
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);

define('SP_REQUEST_URL', ($_SERVER['HTTPS'] ? 'https://' : 'http://') . $_SERVER['HTTP_HOST']);
define('WP_SITEURL', SP_REQUEST_URL);
define('WP_HOME', SP_REQUEST_URL);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
